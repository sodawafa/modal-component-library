import React from 'react'
import './Modal.css'

class Modal extends React.Component {
  state = {
    modal: false,
  }

  constructor (props) {
    super(props)

    if (this.props.modalFn) {
      this.openModal = this.openModal.bind(this)
      this.closeModal = this.closeModal.bind(this)
    } else {
      this.openModal = this.props.openModal
      this.closeModal = this.props.closeModal
    }
  }

  openModal () {
    this.setState({ modal: true })
  }

  closeModal () {
    this.setState({ modal: false })
  }

  render () {
    return (
      <div>
        {this.props.modalBtnOpen &&
        <button className={'modal-btn'} onClick={this.openModal}>open
          modal</button>}
        {
          ((this.props.modalFn && this.state.modal) ||
            (!this.props.modalFn && this.props.modal)) &&
          <div className={'modal-container'}
               onClick={this.closeModal}
          >
            <div className={'modal'}>
              <div className={'modal-body'}>{this.props.children}</div>
              <button className="modal-close"
                      onClick={this.closeModal}>Close
              </button>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default Modal
