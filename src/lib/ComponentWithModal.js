import React from 'react'
import Modal from './Modal'

class ComponentWithModal extends React.Component {

  state = {
    modal: false,
  }
  constructor(props) {
    super(props);
    this.openModal = this.openModal.bind(this)
  }

  openModal () {
    this.setState({ modal: true })
  }

  closeModal () {
    this.setState({ modal: false })
  }

  render () {
    return (
      <div>
      <button className={'modal-btn'} onClick={this.openModal}>open
        modal</button>
      <Modal modalFn={false}
             modalBtnOpen={false}
             modal={this.state.modal}
             openModal={this.openModal.bind(this)}
             closeModal={this.closeModal.bind(this)}>
        <p>component with modal</p>
      </Modal>
      </div>
    )
  }
}

export default ComponentWithModal
