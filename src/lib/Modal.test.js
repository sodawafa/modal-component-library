import React from 'react'
import Modal from './Modal'
import {create} from 'react-test-renderer'

describe('Modal', () => {
  it('renders properly', () => {
    const tree = create(<button className="modal-btn">open
      modal</button>).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
