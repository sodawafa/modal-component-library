import React from 'react'
import { render } from 'react-dom'
import Modal from './lib/Modal'
import ComponentWithModal from './lib/ComponentWithModal'

const App = () => (
  <main>
    <ComponentWithModal/>
    <Modal modalFn={true}
           modalBtnOpen={true}
    >
      <p>modal test</p>
    </Modal>
  </main>
)

render(<App/>, document.getElementById('root'))

export default App
